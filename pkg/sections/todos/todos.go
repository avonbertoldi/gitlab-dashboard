package todos

import (
	"fmt"
	"strings"

	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections"
)

const todosURL = "https://gitlab.com/dashboard/todos"

func init() {
	sections.Register("todos", Run)
}

func Run(api *api.API, config cfg.Config) (string, error) {
	todos, err := api.GetToDos()
	if err != nil {
		return "", fmt.Errorf("failed to get todos: %w", err)
	}

	return renderTodos(todos), nil
}

func renderTodos(todos []api.ToDo) string {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetColumnConfigs([]table.ColumnConfig{
		{Number: 1, WidthMax: 15, WidthMaxEnforcer: render.TextTruncate},
		{Number: 2, WidthMax: 20, WidthMaxEnforcer: render.TextTruncate},
	})
	w.SetTitle(render.Link(fmt.Sprintf("My ToDos (%d)", len(todos)), todosURL))

	w.AppendHeader(table.Row{"Author", "Project", "Item"})

	for _, t := range todos {
		body := strings.ReplaceAll(t.Body, "\n", " ")
		project := ""
		if t.Project != nil {
			project = t.Project.Name
		}
		w.AppendRow(table.Row{
			t.Author.Username,
			project,
			render.Link(body, t.TargetURL),
		})
	}
	return w.Render()
}
