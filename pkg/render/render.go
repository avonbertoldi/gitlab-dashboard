package render

import (
	"fmt"
	nurl "net/url"
	"os"
	"slices"
	"strings"
	"time"

	"github.com/xanzy/go-gitlab"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"golang.org/x/term"
)

const (
	Reset       = "\033[0;0m"
	Red         = "\033[1;31m"
	Green       = "\033[1;32m"
	White       = "\033[1;37m"
	Blue        = "\033[1;34m"
	Yellow      = "\033[1;33m"
	Magenta     = "\033[1;35m"
	Cyan        = "\033[1;36m"
	GreenItalic = "\033[3;32m"
)

func TermWidth() int {
	width, _, err := term.GetSize(int(os.Stdout.Fd()))
	if err != nil {
		panic(err)
	}
	return width
}

func RenderProject(url string) string {
	toks := strings.Split(url, "/")
	return TextEllipsize(toks[len(toks)-4], 15)
}

func Link[T ~int | ~string](text T, url string) string {
	url = nurl.QueryEscape(url)
	switch t := any(text).(type) {
	case int:
		return GreenItalic + fmt.Sprintf("\x1b]8;;%s\x1b\\%6d\x1b]8;;\x1b\\", url, t) + Reset
	case string:
		return fmt.Sprintf("\x1b]8;;%s\x1b\\%s\x1b]8;;\x1b\\", url, t)
	default:
		panic("unexpected object type")
	}
}

var mrStatuses = map[string]string{
	"blocked_status":           "blocked",
	"broken_status":            "borked",
	"checking":                 "-",
	"unchecked":                "-",
	"ci_must_pass":             "ci must pass",
	"ci_still_running":         Green + "merging" + Reset,
	"discussions_not_resolved": "-",
	"draft_status":             "WIP",
	"external_status_checks":   "",
	"mergeable":                Green + "approved" + Reset,
	"not_approved":             Yellow + "not approved" + Reset,
	"not_open":                 "-",
	"policies_denied":          "-",
	"jira_association_missing": "-",
	"merge_request_blocked":    "blocked",
}

func MRStatus(amr *api.AugmentedMR) string {
	return mrStatuses[amr.DetailedMergeStatus]
}

func TextEllipsize(col string, maxLen int) string {
	if len(col) <= maxLen {
		return col
	}
	return col[:maxLen-3] + "..." + Reset
}

func TextTruncate(col string, maxLen int) string {
	if len(col) <= maxLen {
		return col
	}
	return col[:maxLen] + Reset
}

func IssueMilestone(issue *gitlab.Issue) string {
	if issue.Milestone == nil {
		return ""
	}
	return issue.Milestone.Title
}

var customProjectNames = map[string]string{
	"https://gitlab.com/gitlab-org/charts/gitlab-runner":   "gitlab-runner-charts",
	"https://gitlab.com/gitlab-org/security/gitlab-runner": "gitlab-runner-security",
}

func Project(url string) string {
	l := strings.LastIndex(url, "-")
	url = url[0 : l-1]
	if proj, ok := customProjectNames[url]; ok {
		return proj
	}
	toks := strings.Split(url, "/")
	return toks[len(toks)-1]
}

func IssueMRCount(issue *gitlab.Issue) any {
	if issue.MergeRequestCount == 0 {
		return ""
	} else {
		return issue.MergeRequestCount
	}
}

func IssueConfidential(issue *gitlab.Issue) string {
	if issue.Confidential {
		return Red + "confidential" + Reset
	} else {
		return ""
	}
}

func PipelineStatus(amr *api.AugmentedMR) string {
	status := "unknow"
	url := ""
	if amr.Pipeline != nil {
		status = amr.Pipeline.Status
		url = amr.Pipeline.WebURL
	}
	switch status {
	case "failed":
		status = Red + status + Reset
	case "success":
		status = Green + status + Reset
	case "running", "created":
		status = Blue + status + Reset
	default:
		status = Reset + status + Reset
	}
	return Link(status, url)
}

func Title[T *api.AugmentedMR | api.Issue | api.MR](item T, maxWidth int) string {
	switch i := any(item).(type) {
	case api.Issue:
		if i.Confidential {
			return Red + TextEllipsize(i.Title, maxWidth) + Reset
		}
		return TextEllipsize(i.Title, maxWidth)
	case *api.AugmentedMR:
		if isCC(i.Labels) {
			return Magenta + TextEllipsize(i.MR.Title, maxWidth) + Reset
		}
		return TextEllipsize(i.MR.Title, maxWidth)
	case api.MR:
		return i.Title
	default:
		panic("unexpected object type")
	}
}

const (
	readyForReview   = "workflow::ready for review"
	inReview         = "workflow::in review"
	inDev            = "workflow::in dev"
	communityContrib = "Community contribution"
)

//nolint:unused
func isInDev(labels []string) bool          { return slices.Contains(labels, inDev) }
func isReadyForReview(labels []string) bool { return slices.Contains(labels, readyForReview) }
func isInReview(labels []string) bool       { return slices.Contains(labels, inReview) }
func isCC(labels []string) bool             { return slices.Contains(labels, communityContrib) }

func isApprovedByMe(rr *api.AugmentedMR, me string) bool {
	return slices.ContainsFunc(rr.ApprovedBy, func(approver *gitlab.MergeRequestApproverUser) bool {
		return approver.User.Username == me
	})
}

func unresolvedNotes(notes []api.Note) []api.Note {
	return DeleteFunc(notes, func(note api.Note) bool {
		return note.Resolved
	})
}

const (
	oneWeek  = time.Hour * 24 * 7
	twoWeeks = time.Hour * 24 * 14
)

func lastNoteIsMine(notes []api.Note, me string) bool {
	if len(notes) == 0 {
		return false
	}
	last := notes[len(notes)-1]
	return last.Author.Username == me
}

func myNotes(notes []api.Note, me string) []api.Note {
	return DeleteFunc(notes, func(note api.Note) bool {
		return note.Author.Username != me
	})
}

type State string

const (
	StateApproved    State = Green + "approved" + Reset
	StateReviewed    State = Yellow + "reviewed" + Reset
	StateNotReviewed State = Red + "not reviewed" + Reset
)

func MRState(rr *api.AugmentedMR, me string) State {
	if isApprovedByMe(rr, me) {
		return StateApproved
	}

	if isReadyForReview(rr.Labels) || len(myNotes(rr.Notes, me)) == 0 {
		return StateNotReviewed
	}
	return StateReviewed
}

type Court string

const (
	CourtTheris  Court = Green + "theirs" + Reset
	CourtMine    Court = Yellow + "mine" + Reset
	CourtStalled Court = Red + "stalled" + Reset
)

func MRCourt(amr *api.AugmentedMR, me string) Court {
	if amr.Author.Username == me {
		return computeMRCourt(amr, me)
	}
	return computeReviewCourt(amr, me)
}

func computeReviewCourt(amr *api.AugmentedMR, me string) Court {
	if isApprovedByMe(amr, me) {
		if isCC(amr.Labels) {
			return CourtMine
		} else {
			return CourtTheris
		}
	}

	if isReadyForReview(amr.Labels) || isInReview(amr.Labels) {
		return CourtMine
	}

	if lastNoteIsMine(unresolvedNotes(amr.Notes), me) {
		return CourtTheris
	}

	// this is incorrectly set sometimes. let's be conservative and ignore it.
	// if isInDev(rr.Labels) {
	// 	return "theirs"
	// }

	return CourtMine
}

func computeMRCourt(amr *api.AugmentedMR, me string) Court {
	if amr.DetailedMergeStatus == "mergeable" {
		return CourtMine
	}

	if lastNoteIsMine(unresolvedNotes(amr.Notes), me) {
		return CourtTheris
	}

	return CourtMine
}

func Users(users []*gitlab.BasicUser) string {
	result := []string{}
	for _, u := range users {
		result = append(result, u.Username)
	}
	return strings.Join(result, ",")
}

func DaysSinceUpdate(amr *api.AugmentedMR) string {
	if len(amr.Notes) == 0 {
		return "-"
	}
	notes := DeleteFunc(amr.Notes, func(note api.Note) bool {
		return note.Author.Username == "gitlab-bot"
	})
	if len(notes) == 0 {
		return "-"
	}
	last := notes[len(notes)-1]
	since := time.Since(*last.UpdatedAt)
	days := fmt.Sprintf("%.0f", since.Hours()/24)
	url := fmt.Sprintf("%s#note_%d", amr.WebURL, last.ID)

	switch {
	case time.Since(*last.UpdatedAt) < oneWeek:
		return Link(Green+days+Reset, url)
	case time.Since(*last.UpdatedAt) < twoWeeks:
		return Link(Yellow+days+Reset, url)
	default:
		return Link(Red+days+Reset, url)

	}
}

func Age(created time.Time) string {
	return fmt.Sprintf("%.0f", time.Since(created).Hours()/24)
}

func Seconds(s float64) string {
	t := time.Second * time.Duration(s)
	return t.String()
}

func DeleteFunc[S ~[]E, E any](s S, del func(E) bool) S {
	result := S{}
	for _, i := range s {
		if !del(i) {
			result = append(result, i)
		}
	}
	return result
}
