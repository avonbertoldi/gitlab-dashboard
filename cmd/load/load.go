package load

import (
	"errors"
	"os"
	"slices"
	"strings"
	"sync"

	"github.com/jedib0t/go-pretty/v6/table"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
)

type (
	UserWorkLoad struct {
		CommunityMRs, TeamMRs, AuthorMRs, AssignedIssues int
		UserName                                         string
	}
)
type Load struct{}

func (Load) Description() string {
	return "show your team's workload."
}

func (l *Load) Run(a *api.API, config cfg.Config) error {
	m := sync.Mutex{}
	wg := sync.WaitGroup{}
	wg.Add(len(config.Team))
	var errs error
	load := []*UserWorkLoad{}

	for _, member := range config.Team {
		go func(member string) {
			defer wg.Done()
			defer m.Unlock()
			u, e := l.getUserLoad(member, a)
			m.Lock()
			if e != nil {
				errs = errors.Join(errs, e)
			} else {
				load = append(load, u)
			}
		}(member)
	}
	wg.Wait()

	l.renderLoad(load)

	return nil
}

func (l *Load) getUserLoad(member string, a *api.API) (*UserWorkLoad, error) {
	var errs error
	var issues []api.Issue
	var mrs []api.MR
	var reviews []api.MR
	m := sync.Mutex{}
	wg := sync.WaitGroup{}
	wg.Add(3)

	go func() {
		defer wg.Done()
		var err error
		reviews, err = a.GetReviewRequests(member)
		if err != nil {
			m.Lock()
			errs = errors.Join(errs, err)
			m.Unlock()
		}
	}()

	go func() {
		defer wg.Done()
		var err error
		mrs, err = a.GetMergeRequests(member)
		if err != nil {
			m.Lock()
			errs = errors.Join(errs, err)
			m.Unlock()
		}
	}()

	go func() {
		defer wg.Done()
		var err error
		issues, err = a.GetUserIssues(member)
		if err != nil {
			m.Lock()
			errs = errors.Join(errs, err)
			m.Unlock()
		}
	}()

	wg.Wait()
	if errs != nil {
		return nil, errs
	}

	issues = slices.DeleteFunc(issues, func(i api.Issue) bool {
		return strings.Contains(i.WebURL, "team-tasks")
	})

	result := UserWorkLoad{
		AuthorMRs:      len(mrs),
		AssignedIssues: len(issues),
		UserName:       member,
	}

	for _, r := range reviews {
		if slices.Contains(r.Labels, "Community contribution") {
			result.CommunityMRs++
		} else {
			result.TeamMRs++
		}
	}

	return &result, nil
}

func (l *Load) renderLoad(load []*UserWorkLoad) {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SortBy([]table.SortBy{{Number: 1, Mode: table.Asc}})
	w.SetAllowedRowLength(render.TermWidth())
	w.SetOutputMirror(os.Stdout)

	w.SetTitle("Runner Team Workload")
	w.AppendHeader(table.Row{"Member", "Team", "Community", "Author", "Issues"})

	for _, l := range load {
		w.AppendRow(table.Row{
			l.UserName,
			l.TeamMRs,
			l.CommunityMRs,
			l.AuthorMRs,
			l.AssignedIssues,
		})
	}
	w.Render()
}
