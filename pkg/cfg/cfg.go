package cfg

import (
	"bytes"
	"fmt"
	"os"
	"path"

	"gopkg.in/yaml.v3"
)

type User struct {
	Name string `yaml:"username"`
}

type Config struct {
	Team            []string `yaml:"team"`
	Groups []int    `yaml:"groups"`
	Token           string   `yaml:"token"`
	DefaultSections []string `yaml:"sections"`
	User            `yaml:",inline"`
}

func Load(filename string) (Config, error) {
	result := Config{}
	data, err := os.ReadFile(filename)
	if err != nil {
		return result, fmt.Errorf("failed to read file %s: %w", filename, err)
	}

	enc := yaml.NewDecoder(bytes.NewReader(data))
	enc.KnownFields(true)

	return result, enc.Decode(&result)
}

func DefaultConfigPath() (string, error) {
	exec, err := os.Executable()
	if err != nil {
		return "", err
	}
	return path.Join(path.Dir(exec), "config.yaml"), nil
}
