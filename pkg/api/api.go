package api

import (
	"cmp"
	"errors"
	"fmt"
	"slices"
	"sync"

	"github.com/xanzy/go-gitlab"
)

type (
	MR                = *gitlab.MergeRequest
	Approvals         = *gitlab.MergeRequestApprovals
	Note              = *gitlab.Note
	ToDo              = *gitlab.Todo
	Issue             = *gitlab.Issue
	User              = *gitlab.User
	ListIssuesOptions = *gitlab.ListIssuesOptions
	Job               = *gitlab.Job
	Jobs              = []Job

	AugmentedMR struct {
		MR
		Approvals
		Notes []Note
	}

	API struct {
		api *gitlab.Client
	}
)

func New(apiToken string) (*API, error) {
	api, err := gitlab.NewClient(apiToken)
	if err != nil {
		return nil, fmt.Errorf("failed to create gitlab api client: %w", err)
	}

	return &API{api: api}, nil
}

func (a *API) GetUserIssues(username string) ([]Issue, error) {
	issues, _, err := a.api.Issues.ListIssues(&gitlab.ListIssuesOptions{
		Scope:            gitlab.Ptr("all"),
		State:            gitlab.Ptr("opened"),
		AssigneeUsername: gitlab.Ptr(username),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}, nil)
	return issues, err
}

func (a *API) GetIssues(opts ListIssuesOptions) ([]Issue, error) {
	issues, _, err := a.api.Issues.ListIssues(opts, nil)
	return issues, err
}

func (a *API) GetMergeRequestPipelines(pid any, mrID int) ([]*gitlab.PipelineInfo, error) {
	pipelines, _, err := a.api.MergeRequests.ListMergeRequestPipelines(pid, mrID)
	return pipelines, err
}

func (a *API) GetMergeRequestApprovals(pid any, mrID int) (*gitlab.MergeRequestApprovals, error) {
	approvals, _, err := a.api.MergeRequests.GetMergeRequestApprovals(pid, mrID)
	return approvals, err
}

// GET /projects/:id/merge_requests/:merge_request_iid/notes?sort=asc&order_by=updated_at
func (a *API) GetMergeRequestComments(pid any, mrID int) ([]*gitlab.Note, error) {
	var result []*gitlab.Note
	page := 1
	for {
		notes, resp, err := a.api.Notes.ListMergeRequestNotes(pid, mrID, &gitlab.ListMergeRequestNotesOptions{
			OrderBy: gitlab.Ptr("created_at"),
			Sort:    gitlab.Ptr("asc"),
			ListOptions: gitlab.ListOptions{
				PerPage: 100,
				Page:    page,
			},
		})
		if err != nil {
			return nil, err
		}
		result = append(result, notes...)
		if resp.TotalPages == resp.CurrentPage {
			break
		}
		page++
	}
	return result, nil
}

func (a *API) GetReviewRequests(username string) ([]*gitlab.MergeRequest, error) {
	return a.getMergeRequests(&gitlab.ListMergeRequestsOptions{
		Scope:            gitlab.Ptr("all"),
		State:            gitlab.Ptr("opened"),
		ReviewerUsername: gitlab.Ptr(username),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	})
}

func (a *API) GetMergeRequests(username string) ([]*gitlab.MergeRequest, error) {
	return a.getMergeRequests(&gitlab.ListMergeRequestsOptions{
		Scope:          gitlab.Ptr("all"),
		State:          gitlab.Ptr("opened"),
		AuthorUsername: gitlab.Ptr(username),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	})
}

func (a *API) getMergeRequests(opts *gitlab.ListMergeRequestsOptions) ([]*gitlab.MergeRequest, error) {
	mrs, _, err := a.api.MergeRequests.ListMergeRequests(opts)
	return mrs, err
}

func (a *API) AugmentMergeRequests(mrs []MR) ([]*AugmentedMR, error) {
	result := make([]*AugmentedMR, 0, len(mrs))
	wg := sync.WaitGroup{}
	wg.Add(len(mrs))
	var allErrs error
	for _, mr := range mrs {
		amr := AugmentedMR{MR: mr}
		result = append(result, &amr)
		go func(amr *AugmentedMR) {
			defer wg.Done()
			allErrs = errors.Join(allErrs, a.augmentMergeRequest(amr))
		}(&amr)
	}

	wg.Wait()
	return result, allErrs
}

func (a *API) augmentMergeRequest(amr *AugmentedMR) (allErrs error) {
	wg := sync.WaitGroup{}
	wg.Add(3)
	go func() {
		defer wg.Done()
		approvals, err := a.GetMergeRequestApprovals(amr.MR.ProjectID, amr.MR.IID)
		if err == nil {
			amr.Approvals = approvals
		}
		allErrs = errors.Join(allErrs, err)
	}()

	go func() {
		defer wg.Done()
		notes, err := a.GetMergeRequestComments(amr.MR.ProjectID, amr.MR.IID)
		if err == nil {
			amr.Notes = notes
		}
		allErrs = errors.Join(allErrs, err)
	}()

	go func() {
		defer wg.Done()
		pipelines, err := a.GetMergeRequestPipelines(amr.MR.ProjectID, amr.MR.IID)
		if err == nil && len(pipelines) != 0 {
			slices.SortFunc(pipelines, func(a, b *gitlab.PipelineInfo) int {
				return cmp.Compare(a.ID, b.ID)
			})
			amr.Pipeline = pipelines[len(pipelines)-1]
		}
		allErrs = errors.Join(allErrs, err)
	}()

	wg.Wait()
	return allErrs
}

func (a *API) GetToDos() ([]ToDo, error) {
	todos, _, err := a.api.Todos.ListTodos(&gitlab.ListTodosOptions{
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	})
	return todos, err
}

func (a *API) GetRunnerCCMRs(noReviewer bool, gids ...int) ([]MR, error) {
	wg := sync.WaitGroup{}
	wg.Add(len(gids))
	mu := sync.Mutex{}
	var errs error
	var result []MR

	for _, gid := range gids {
		go func(gid int) {
			defer wg.Done()
			mrs, err := a.getGroupMRs(noReviewer, gid)
			mu.Lock()
			errs = errors.Join(errs, err)
			result = append(result, mrs...)
			mu.Unlock()
		}(gid)
	}

	wg.Wait()
	return result, errs
}

func (a *API) getGroupMRs(noReviewer bool, gid int) ([]MR, error) {
	req := gitlab.ListGroupMergeRequestsOptions{
		Labels: &gitlab.LabelOptions{"Community contribution", "group::runner"},
		Scope:  gitlab.Ptr("all"),
		State:  gitlab.Ptr("opened"),
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
	if noReviewer {
		req.ReviewerID = gitlab.ReviewerID(gitlab.UserIDNone)
	}

	mrs, _, err := a.api.MergeRequests.ListGroupMergeRequests(gid, &req)
	return mrs, err
}

func (a *API) GetUser(username string) (User, error) {
	users, _, err := a.api.Users.ListUsers(&gitlab.ListUsersOptions{Username: gitlab.Ptr(username)})
	if err != nil {
		return nil, err
	}

	if len(users) != 1 {
		return nil, fmt.Errorf("no such user %s", username)
	}

	return users[0], nil
}

func (a *API) GetProjectFailedJobs(projectID string) (Jobs, error) {
	const pages = 3
	opt := gitlab.ListJobsOptions{
		Scope: &[]gitlab.BuildStateValue{gitlab.Failed},
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	var result Jobs

	for i := 0; i < pages; i++ {
		opt.Page = i
		pjobs, _, err := a.api.Jobs.ListProjectJobs(projectID, &opt)
		if err != nil {
			return nil, err
		}
		result = append(result, pjobs...)
	}
	return result, nil
}

func (a *API) GetProjectJobs(projectID string, count int, failed bool) (Jobs, error) {
	pages := count / 100
	scope := []gitlab.BuildStateValue{}
	if failed {
		scope = append(scope, gitlab.Failed)
	}
	opt := gitlab.ListJobsOptions{
		Scope: &scope,
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}

	var result Jobs

	for i := 0; i < pages; i++ {
		opt.Page = i
		pjobs, _, err := a.api.Jobs.ListProjectJobs(projectID, &opt)
		if err != nil {
			return nil, err
		}
		result = append(result, pjobs...)
	}
	return result, nil
}
