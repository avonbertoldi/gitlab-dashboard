package jobs

import (
	"fmt"
	"os"
	"sort"

	"github.com/jedib0t/go-pretty/v6/table"

	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
)

type Jobs struct {
	ProjectID string `arg:"positional" default:"250833" help:"The GitLab Project for which to list failed jobs. Defaults to 250833."`
	JobName   string `arg:"-j,--job-name" help:"The name of the job for which to list run urls."`
	All       bool   `arg:"-a,--all" help:"Include results for all jobs, nut just failed ones."`
	// Reason    string `arg:"-r,--failure-reason" help:"Show jobs that have failed for this reason."`
}

func (Jobs) Description() string {
	return "list recently failed jobs and their count"
}

func (j *Jobs) Run(api *api.API, config cfg.Config) error {
	jobs, err := api.GetProjectJobs(j.ProjectID, 300, !j.All)
	if err != nil {
		return fmt.Errorf("failed to get jobs for project %q: %w", j.ProjectID, err)
	}

	if j.JobName == "" {
		j.renderSummary(jobs)
	} else {
		j.renderJob(j.JobName, jobs)
	}
	return nil
}

type job struct {
	count int
	times []float64
}

func (j *Jobs) renderSummary(jobs api.Jobs) {
	// collate results
	jobCountByName := map[string]*job{}
	for _, j := range jobs {
		if _, ok := jobCountByName[j.Name]; !ok {
			jobCountByName[j.Name] = &job{}
		}
		jobCountByName[j.Name].count++
		jobCountByName[j.Name].times = append(jobCountByName[j.Name].times, j.Duration)
	}

	// sort results by count
	jobNames := []string{}
	for job := range jobCountByName {
		jobNames = append(jobNames, job)
	}
	sort.SliceStable(jobNames, func(i, j int) bool {
		return jobCountByName[jobNames[i]].count > jobCountByName[jobNames[j]].count
	})

	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetOutputMirror(os.Stdout)

	w.SetTitle("Latest %d failed jobs for project %q", len(jobs), j.ProjectID)
	w.AppendHeader(table.Row{"Count", "Job Name", "Mean Duration"})

	for _, job := range jobNames {
		w.AppendRow(table.Row{
			jobCountByName[job].count,
			job,
			render.Seconds(mean(jobCountByName[job].times)),
		})
	}

	w.Render()
}

func mean(data []float64) float64 {
	if len(data) == 0 {
		return 0
	}
	var sum float64
	for _, d := range data {
		sum += float64(d)
	}
	return sum / float64(len(data))
}

func (j *Jobs) renderJob(jobName string, jobs api.Jobs) {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetOutputMirror(os.Stdout)

	w.AppendHeader(table.Row{"Job ID", "Duration", "Failure Reason", "Owner"})

	count := 0
	for _, j := range jobs {
		if j.Name != jobName {
			continue
		}
		count++
		w.AppendRow(table.Row{
			render.Link(j.ID, j.WebURL),
			render.Seconds(j.Duration),
			j.FailureReason,
			j.User.Username,
		})
	}

	w.SetTitle("%d failed instances of job %q", count, jobName)
	w.Render()
}
