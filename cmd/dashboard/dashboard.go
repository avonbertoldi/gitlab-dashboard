package dashboard

import (
	"errors"
	"fmt"
	"slices"
	"sync"

	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections"
	_ "gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections/issues"
	_ "gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections/mrs"
	_ "gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections/reviews"
	_ "gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections/todos"
)

type Dashboard struct {
	Sections []string `arg:"-s,--sections" help:"sections to display. valid values are (mrs, reviews, issues, todos). defaults to the sections in the configuration file."`
	User     string   `arg:"-u,--user" help:"defaults to the user in the config file (does not apply to todos)."`
}

func (Dashboard) Description() string {
	return "show your personal dashboard with the specified sections."
}

func (d *Dashboard) init(config cfg.Config) {
	if len(d.Sections) == 0 {
		d.Sections = config.DefaultSections
	}

	registeredSections := sections.Get()
	d.Sections = slices.DeleteFunc(d.Sections, func(s string) bool {
		_, ok := registeredSections[s]
		if !ok {
			fmt.Println("skipping unsupported section", s)
		}
		return !ok
	})
}

func (d *Dashboard) Run(api *api.API, config cfg.Config) error {
	d.init(config)

	if d.User != "" {
		config.User.Name = d.User
	}

	registeredSections := sections.Get()

	wg := sync.WaitGroup{}
	wg.Add(len(d.Sections))

	result := map[int]string{}
	var errs error

	for i, s := range d.Sections {
		go func(i int, s string) {
			defer wg.Done()

			res, err := registeredSections[s](api, config)
			if err != nil {
				errs = errors.Join(errs, err)
			}

			result[i] = res
		}(i, s)
	}

	wg.Wait()

	for i := range d.Sections {
		fmt.Println("")
		fmt.Println(result[i])
	}
	return errs
}
