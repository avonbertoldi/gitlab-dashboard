# GitLab Dashboard +

A tool for working developers using GitLab. The implementation is _highly_ tailored to the [runner team](https://handbook.gitlab.com/handbook/engineering/development/ops/verify/runner/), but could easily be adapted to work
for other teams, or individual developers.

# Overview

`gitlab-dashboard` has four subcommands; `dashboard`, `ccmrs`, `cves`, and `load`. `dashboard` is the default command if
none is specified.

```sh
> gitlab-dashboard -h
Usage: gitlab-dashboard [--token TOKEN] [--config CONFIG] <command> [<args>]

Options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location
  --help, -h             display this help and exit

Commands:
  dashboard              show your personal dashboard with the specified sections (default command).
  ccmrs                  list open community MRs assigned to the runner team.
  load                   show your team's workload.
  cves                   list active CVE vulnerability issues.
```
## Commands

### dashboard

`dashboard` summarizes your "sources of work".
```sh
> gitlab-dashboard dashboard -h
Usage: gitlab-dashboard dashboard [--sections SECTIONS] [--user USER]

Options:
  --sections SECTIONS, -s SECTIONS
                         sections to display. valid values are (mrs, reviews, issues, todos). defaults to the sections in the configuration file.
  --user USER, -u USER   defaults to the user in the config file (does not apply to todos).

Global options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location
  --help, -h             display this help and exit

```

The are are four source of work:

* mrs: MRs where the specified user is the author.
* Reviews: MRs where the specified user is a reviewer.
* Issues: Issues assigned to the specified user.
* todos: ToDos for the user associated with the token. It is not possible to view another user's ToDos.

Any combination of the four sources and be displayed. The default sources are specified in the configuration file.

### cmrs

`ccmrs` lists the open community contribution MRs for the runner team to review.

```sh
> gitlab-dashboard ccmrs -h
Usage: gitlab-dashboard ccmrs [--all]

Options:
  --all, -a              Include community MRs with assigned reviewers. [default: false]

Global options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location
  --help, -h             display this help and exit
```


### load

`load` lists the assigned MRs, review requests and issues for each member of the team.

```sh
> gitlab-dashboard load -h
Usage: gitlab-dashboard load

Global options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file.
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location.
  --help, -h             display this help and exit
```

### cves

cves lists the active (opened and without an open deviation request) issues for CVE vulnerabilities assigned to the
runner team.

```sh
Usage: gitlab-dashboard cves [--severities SEVERITIES]

Options:
  --severities SEVERITIES, -s SEVERITIES
                         Severities to display. valid values are (critical, high, medium, low). defaults to (critical, high)

Global options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file.
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location.
  --help, -h             display this help and exit
```

### wrangler

`wrangler` lists issues created within the last seen days, filter by the specified (or default) labels. 

```sh
/gitlab-dashboard wrangler -h
Usage: gitlab-dashboard wrangler [--labels LABELS] [--age AGE]

Options:
  --labels LABELS, -l LABELS
                         Filter issues by lables. defaults to {'group::runner', 'Help group::runner'}
  --age AGE, -a AGE      Max age, in days, of issues to include [default: 7]

Global options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file.
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location.
  --help, -h             display this help and exit
```

### jobs

`job` lists CI jobs for the specified project, optionally filtering them based on completion status, and grouping them
by name.

```sh
gitlab-dashboard jobs -h
Usage: gitlab-dashboard jobs [--job-name JOB-NAME] [--all] [PROJECTID]

Positional arguments:
  PROJECTID              The GitLab Project for which to list failed jobs. Defaults to 250833.

Options:
  --job-name JOB-NAME, -j JOB-NAME
                         The name of the job for which to list run urls.
  --all, -a              Include results for all jobs, nut just failed ones.

Global options:
  --token TOKEN, -t TOKEN
                         The API token. defaults to the token in the configuration file.
  --config CONFIG, -c CONFIG
                         path to config file. defaults to the binary's location.
  --help, -h             display this help and exit
```

# Config File

The config file has five sections:

* team: The GitLab usernames of the members of your team, excluding yourself.
* groups: The GitLab groups to which you belong.
* token: Your GitLab API token.
* username: Your GitLab username.
* sections: The default sections to display in the `dashboard` command.

Unless specified via `--config`, the command will look for the config file in the same path as the binary itself.

# Installation

_For now_, either clone the repo and run `make`, or install it via `go install
gitlab.com/avonbertoldi/gitlab-dashboard`. In any case, don't forget to create the config file in the right place.
