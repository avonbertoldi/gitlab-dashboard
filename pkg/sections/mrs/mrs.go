package mrs

import (
	"cmp"
	"fmt"
	"slices"

	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
)

const mrsURL = "https://gitlab.com/dashboard/merge_requests?assignee_username=%s"

func init() {
	sections.Register("mrs", Run)
}

func Run(api *api.API, config cfg.Config) (string, error) {
	mrs, err := api.GetMergeRequests(config.User.Name)
	if err != nil {
		return "", fmt.Errorf("failed to get merge requests: %w", err)
	}

	amrs, err := api.AugmentMergeRequests(mrs)
	if err != nil {
		return "", fmt.Errorf("failed to augment merge requests: %w", err)
	}

	return renderMergeRequests(amrs, config.Name), nil
}

func renderMergeRequests(amrs []*api.AugmentedMR, me string) string {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetColumnConfigs([]table.ColumnConfig{
		{Number: 6, Align: text.AlignRight},
	})

	w.SetTitle(render.Link(fmt.Sprintf("My Merge Requests (%d)", len(amrs)), fmt.Sprintf(mrsURL, me)))
	w.AppendHeader(table.Row{"Project", "ID", "Title", "Branch", "Status", "Court", "Last\nUpdate", "Pipeline"})

	slices.SortFunc(amrs, func(a, b *api.AugmentedMR) int {
		return cmp.Or(
			cmp.Compare(render.Project(a.WebURL), render.Project(b.WebURL)),
			cmp.Compare(a.MR.IID, b.MR.IID))
	})

	for _, amr := range amrs {
		w.AppendRow(table.Row{
			render.Project(amr.MR.WebURL),
			render.Link(amr.MR.IID, amr.MR.WebURL),
			render.Title(amr, 75),
			render.TextTruncate(amr.SourceBranch, 30),
			render.MRStatus(amr),
			render.MRCourt(amr, me),
			render.DaysSinceUpdate(amr),
			render.PipelineStatus(amr),
			// render.MRState(amr, member.Name),
			// render.MRCourt(amr, member.Name),
		})
	}
	return w.Render()
}
