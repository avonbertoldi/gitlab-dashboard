package wrangler

import (
	"cmp"
	"fmt"
	"os"
	"slices"
	"time"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
)

var (
	defaultLabels = []string{"group::runner", "Help group::runner"}

	issueOpts = &gitlab.ListIssuesOptions{
		Scope:     gitlab.Ptr("all"),
		State:     gitlab.Ptr("opened"),
		NotLabels: &gitlab.LabelOptions{"FedRAMP::Vulnerability"},
		ListOptions: gitlab.ListOptions{
			PerPage: 100,
			Page:    1,
		},
	}
)

type Wrangler struct {
	Labels []string `arg:"-l,--labels" help:"Filter issues by lables. defaults to {'group::runner', 'Help group::runner'}"`
	Age    int      `arg:"-a,--age" help:"Max age, in days, of issues to include" default:"7"`
}

func (w *Wrangler) Run(api *api.API, config cfg.Config) error {
	if len(w.Labels) == 0 {
		w.Labels = defaultLabels
	}

	maxAge := w.Age * -1

	lastWeek := time.Now().AddDate(0, 0, maxAge).Round(0)

	issues := []*gitlab.Issue{}

	for _, l := range w.Labels {
		opts := issueOpts
		opts.CreatedAfter = &lastWeek
		opts.Labels = &gitlab.LabelOptions{l}

		lissues, err := api.GetIssues(issueOpts)
		if err != nil {
			return fmt.Errorf("failed to get issues for label %q: %w", l, err)
		}

		issues = append(issues, lissues...)

	}

	issues = slices.DeleteFunc(issues, func(i *gitlab.Issue) bool {
		switch render.Project(i.WebURL) {
		case "gitlab-OKRs", "team-tasks", "triage-reports":
			return true
		default:
			return slices.Contains(config.Team, i.Author.Username)
		}
	})

	w.render(&lastWeek, issues)
	return nil
}

func (c *Wrangler) render(since *time.Time, issues []api.Issue) {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetColumnConfigs([]table.ColumnConfig{
		{Number: 3, Align: text.AlignRight},
	})
	w.SetOutputMirror(os.Stdout)

	link := ""

	_, month, day := since.Date()

	title := render.Link(fmt.Sprintf(" New Issues since %s %d (%d)", month, day, len(issues)), link)
	fmt.Println(title)
	// w.SetTitle(link) does not work
	w.AppendHeader(table.Row{"Project", "ID", "Age", "Title"})

	slices.SortFunc(issues, func(a, b api.Issue) int {
		return cmp.Or(
			cmp.Compare(render.Project(a.WebURL), render.Project(b.WebURL)),
			cmp.Compare(a.IID, b.IID))
	})

	for _, issue := range issues {
		w.AppendRow(table.Row{
			render.Project(issue.WebURL),
			render.Link(issue.IID, issue.WebURL),
			render.Age(*issue.CreatedAt),
			render.Title(issue, 250),
		})
	}
	w.Render()
	fmt.Println()
}
