module gitlab.com/avonbertoldi/gitlab-dashboard

go 1.23

require (
	github.com/alexflint/go-arg v1.5.1
	github.com/jedib0t/go-pretty/v6 v6.6.1
	github.com/xanzy/go-gitlab v0.113.0
	golang.org/x/sync v0.8.0
	golang.org/x/term v0.25.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/alexflint/go-scalar v1.2.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.7 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	golang.org/x/oauth2 v0.23.0 // indirect
	golang.org/x/sys v0.26.0 // indirect
	golang.org/x/time v0.7.0 // indirect
)
