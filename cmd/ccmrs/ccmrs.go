package ccmrs

import (
	"cmp"
	"fmt"
	"os"
	"slices"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
)

const (
	CCMRsURL = `https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community contribution&label_name[]=group::runner`
)

type CCMRs struct {
	All bool `arg:"-a,--all" help:"Include community MRs with assigned reviewers." default:"false"`
}

func (CCMRs) Description() string {
	return "list open community MRs assigned to the runner team"
}

func (c *CCMRs) Run(api *api.API, config cfg.Config) error {
	mrs, err := api.GetRunnerCCMRs(!c.All, config.Groups...)
	if err != nil {
		return fmt.Errorf("failed to get merge requests: %w", err)
	}
	amrs, err := api.AugmentMergeRequests(mrs)
	if err != nil {
		return fmt.Errorf("failed to augment merge requests: %w", err)
	}

	c.render(amrs)
	return nil
}

func (c *CCMRs) render(amrs []*api.AugmentedMR) {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetColumnConfigs([]table.ColumnConfig{
		{Number: 6, Align: text.AlignRight},
		{Number: 7, Align: text.AlignRight},
	})
	w.SetOutputMirror(os.Stdout)

	link := CCMRsURL
	if !c.All {
		link += `&reviewer_id=None`
	}
	title := render.Link(fmt.Sprintf(" Runner Community Merge Requests (%d)", len(amrs)), link)
	fmt.Println(title)
	// w.SetTitle(link) does not work
	w.AppendHeader(table.Row{"Project", "ID", "Author", "Title", "Reviewer", "Age", "Last\nUpdate"})

	slices.SortFunc(amrs, func(a, b *api.AugmentedMR) int {
		return cmp.Or(
			cmp.Compare(render.Project(a.WebURL), render.Project(b.WebURL)),
			cmp.Compare(a.MR.IID, b.MR.IID))
	})

	for _, amr := range amrs {
		w.AppendRow(table.Row{
			render.Project(amr.WebURL),
			render.Link(amr.MR.IID, amr.WebURL),
			amr.Author.Username,
			render.Title(amr, 250),
			render.Users(amr.Reviewers),
			render.Age(*amr.MR.CreatedAt),
			render.DaysSinceUpdate(amr),
		})
	}
	w.Render()
}
