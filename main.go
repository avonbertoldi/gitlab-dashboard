package main

import (
	"log"

	"gitlab.com/avonbertoldi/gitlab-dashboard/cmd/ccmrs"
	"gitlab.com/avonbertoldi/gitlab-dashboard/cmd/cves"
	"gitlab.com/avonbertoldi/gitlab-dashboard/cmd/dashboard"
	"gitlab.com/avonbertoldi/gitlab-dashboard/cmd/jobs"
	"gitlab.com/avonbertoldi/gitlab-dashboard/cmd/load"
	"gitlab.com/avonbertoldi/gitlab-dashboard/cmd/wrangler"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"

	"github.com/alexflint/go-arg"
)

type (
	App struct {
		APIToken string `arg:"-t,--token" help:"The API token. defaults to the token in the configuration file."`
		Config   string `arg:"-c,--config" help:"path to config file. defaults to the binary's location."`

		Dash     *dashboard.Dashboard `arg:"subcommand:dashboard" help:"show your personal dashboard with the specified sections (default command)."`
		CCMRs    *ccmrs.CCMRs         `arg:"subcommand:ccmrs" help:"list open community MRs assigned to the runner team."`
		Load     *load.Load           `arg:"subcommand:load" help:"show your team's workload."`
		CVEs     *cves.CVEs           `arg:"subcommand:cves" help:"list active CVE vulnerability issues."`
		Wrangler *wrangler.Wrangler   `arg:"subcommand:wrangler" help:"Bug search for Bug Wrangler duty."`
		Jobs     *jobs.Jobs           `arg:"subcommand:jobs" help:"list recently failed jobs and their count."`

		api *api.API
		cfg cfg.Config
		p   *arg.Parser
	}

	Runnable interface {
		Run(*api.API, cfg.Config) error
	}
)

func main() {
	app := App{}

	if err := app.Init(); err != nil {
		log.Fatalln(err)
	}

	if err := app.Run(); err != nil {
		log.Fatalln(err)
	}
}

func (a *App) Init() error {
	a.p = arg.MustParse(a)

	var err error

	if a.Config == "" {
		a.Config, err = cfg.DefaultConfigPath()
		if err != nil {
			log.Fatalln(err.Error())
		}
	}

	a.cfg, err = cfg.Load(a.Config)
	if err != nil {
		return err
	}
	if a.APIToken != "" {
		a.cfg.Token = a.APIToken
	}

	a.api, err = api.New(a.cfg.Token)
	if err != nil {
		return err
	}

	return nil
}

func (a *App) Run() error {
	var cmd Runnable
	if a.p.Subcommand() == nil {
		cmd = &dashboard.Dashboard{}
	} else {
		cmd = a.p.Subcommand().(Runnable)
	}
	return cmd.Run(a.api, a.cfg)
}
