APP=gitlab-dashboard

SRCS=$(shell find . -name "*.go")

$(APP): $(SRCS) go.mod go.sum
	go build .

clean:
	rm -f $(APP)

lint:
	golangci-lint run .
