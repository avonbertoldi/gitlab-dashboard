package issues

import (
	"cmp"
	"fmt"
	"slices"

	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/sections"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
)

const issuesURL = "https://gitlab.com/dashboard/issues?sort=priority_desc&state=opened&assignee_username[]=%s"

func init() {
	sections.Register("issues", Run)
}

func Run(a *api.API, config cfg.Config) (string, error) {
	issues, err := getIssues(a, config.User)
	if err != nil {
		return "", fmt.Errorf("failed to get issues: %w", err)
	}
	return renderIssues(issues, config.Name), nil
}

func getIssues(api *api.API, member cfg.User) ([]api.Issue, error) {
	issues, err := api.GetUserIssues(member.Name)
	if err != nil {
		return nil, err
	}
	return issues, nil
}

func renderIssues(issues []api.Issue, me string) string {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetAllowedRowLength(render.TermWidth())
	w.SetColumnConfigs([]table.ColumnConfig{
		{Number: 1, WidthMax: 25, WidthMaxEnforcer: render.TextEllipsize},
		{Number: 2, Align: text.AlignRight},
		{Number: 4, Align: text.AlignRight, WidthMax: 9, WidthMaxEnforcer: render.TextTruncate},
		{Number: 6, Align: text.AlignRight},
	})
	w.SetTitle(render.Link(fmt.Sprintf("My Issues (%d)", len(issues)), fmt.Sprintf(issuesURL, me)))
	w.AppendHeader(table.Row{"Project", "ID", "Title", "Milestone", "Notes", "MRS"})

	slices.SortFunc(issues, func(a, b api.Issue) int {
		return cmp.Or(
			cmp.Compare(render.Project(a.WebURL), render.Project(b.WebURL)),
			cmp.Compare(a.IID, b.IID))
	})

	for _, issue := range issues {
		w.AppendRow(table.Row{
			render.Project(issue.WebURL),
			render.Link(issue.IID, issue.WebURL),
			render.Title(issue, 120),
			render.IssueMilestone(issue),
			issue.UserNotesCount,
			render.IssueMRCount(issue),
		})
	}
	return w.Render()
}
