package cves

import (
	"cmp"
	"fmt"
	"os"
	"slices"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/jedib0t/go-pretty/v6/text"
	"github.com/xanzy/go-gitlab"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/render"
	"golang.org/x/sync/errgroup"
)

const (
	CCMRsURL = `https://gitlab.com/groups/gitlab-org/-/merge_requests?scope=all&state=opened&label_name[]=Community contribution&label_name[]=group::runner`
)

type Issue = *gitlab.Issue

type CVEs struct {
	Severities []string `arg:"-s,--severities" help:"Severities to display. valid values are (critical, high, medium, low). defaults to (critical, high)"`
}

func (CVEs) Description() string {
	return "list open CVE issues without open or accepted deviation requests"
}

var (
	baseLabels     = gitlab.LabelOptions{"group::runner", "FedRAMP::Vulnerability"}
	severityLabels = map[string]string{
		"critical": "Vulnerability Impact::Scan Severity::Critical",
		"high":     "Vulnerability Impact::Scan Severity::High",
		"medium":   "Vulnerability Impact::Scan Severity::Medium",
		"low":      "Vulnerability Impact::Scan Severity::Low",
	}
	defaultSeverities = []string{"critical", "high"}
)

var issueOpts = &gitlab.ListIssuesOptions{
	Scope:     gitlab.Ptr("all"),
	State:     gitlab.Ptr("opened"),
	NotLabels: &gitlab.LabelOptions{"FedRAMP::DR Status::Accepted", "FedRAMP::DR Status::Open"},
	ListOptions: gitlab.ListOptions{
		PerPage: 100,
		Page:    1,
	},
}

func (c *CVEs) Run(api *api.API, config cfg.Config) error {
	if len(c.Severities) == 0 {
		c.Severities = defaultSeverities
	}

	results := map[string][]Issue{}
	eg := errgroup.Group{}

	for _, sev := range c.Severities {
		eg.Go(func() error {
			opts := issueOpts
			sevLabel, ok := severityLabels[sev]
			if !ok {
				return fmt.Errorf("invalid severity %q", sev)
			}
			allLabels := append(baseLabels, sevLabel)
			opts.Labels = &allLabels

			issues, err := api.GetIssues(issueOpts)
			if err != nil {
				return fmt.Errorf("failed to get %q issues: %w", sev, err)
			}

			results[sev] = issues
			return nil
		})
	}

	if err := eg.Wait(); err != nil {
		return err
	}

	for _, sev := range c.Severities {
		c.render(sev, results[sev])
	}
	return nil
}

func (c *CVEs) render(severity string, issues []Issue) {
	w := table.NewWriter()
	w.Style().Options.DrawBorder = false
	w.Style().Options.SeparateColumns = false
	w.SetColumnConfigs([]table.ColumnConfig{
		{Number: 3, Align: text.AlignRight},
	})
	w.SetAllowedRowLength(render.TermWidth())
	w.SetOutputMirror(os.Stdout)

	link := CCMRsURL

	title := render.Link(fmt.Sprintf(" Active %q CVE Issues (%d)", severity, len(issues)), link)
	fmt.Println(title)
	// w.SetTitle(link) does not work
	w.AppendHeader(table.Row{"Project", "ID", "Age", "Title"})

	slices.SortFunc(issues, func(a, b Issue) int {
		return cmp.Or(
			cmp.Compare(render.Project(a.WebURL), render.Project(b.WebURL)),
			cmp.Compare(a.IID, b.IID))
	})

	for _, issue := range issues {
		w.AppendRow(table.Row{
			render.Project(issue.WebURL),
			render.Link(issue.IID, issue.WebURL),
			render.Age(*issue.CreatedAt),
			render.Title(issue, 250),
		})
	}
	w.Render()
	fmt.Println()
}
