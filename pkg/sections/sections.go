package sections

import (
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/api"
	"gitlab.com/avonbertoldi/gitlab-dashboard/pkg/cfg"
)

type (
	SectionFunc func(*api.API, cfg.Config) (string, error)
	Section     struct {
		Run  SectionFunc
		Name string
	}
)

var sections = map[string]SectionFunc{}

func Register(name string, sf SectionFunc) {
	sections[name] = sf
}

func Get() map[string]SectionFunc {
	return sections
}
